﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.DTO
{
    public class CreateEmployee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }

    public class UpdateEmployee : CreateEmployee
    {
        public Guid Id { get; set; }
        public List<string> Role { get; set; }
    }
}