﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.DTO;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = FakeDataFactory.Employees;

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = FakeDataFactory.Employees.FirstOrDefault(x => x.Id == id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }


        /// <summary>
        /// Создание нового сотрудника
        /// </summary>
        /// <param name="employee">Данные сотрудника</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployee(CreateEmployee employee)
        {
            if (employee is null) return null;

            var role = FakeDataFactory.Roles.FirstOrDefault(x=>
                x.Id == Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"));

            var entity = new Employee()
            {
                Id = Guid.NewGuid(),
                Email = employee.Email,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Roles = new List<Role>() {role}
            };
            FakeDataFactory.Employees.Add(entity);

            return entity.Id;
        }

        [HttpDelete]
        public async Task DeleteEmployee(string id) => 
            FakeDataFactory.Employees.Remove(
                FakeDataFactory.Employees.FirstOrDefault(x => x.Id == Guid.Parse(id)));

        [HttpPut]
        public async Task<bool> UpdateEmployee(UpdateEmployee update)
        {
            var employee = FakeDataFactory.Employees.FirstOrDefault(x => x.Id == update.Id);
            if (employee is null) return false;

            var roles = new List<Role>();
            foreach (var role in update.Role)
            {
                var entity = FakeDataFactory.Roles.FirstOrDefault(x => x.Id == Guid.Parse(role));
                if (entity is null) continue;
                roles.Add(entity);
            }

            employee.FirstName = update.FirstName;
            employee.LastName = update.LastName;
            employee.Email = update.Email;
            employee.Roles = roles;

            return true;
        }
        
    }
}